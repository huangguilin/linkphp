<?php
namespace app\http\controller;
use framework\Controller;
use framework\Application;
use Closure;
use linkphp\event\Event;
use Db;
use Console;
use Config;
use framework\Exception;
use linkphp\http\HttpRequest;
use phprpc\PhpRpcClient;
use Router;
use Validator;
use linkphp\page\Paginator;

class Home extends Controller
{

//    public function __construct(Controller $controller, HttpRequest $httpRequest)
//    {
//        parent::__construct($httpRequest);
//        dump($controller);
//    }

    public function main1(HttpRequest $httpRequest)
    {
        dump($httpRequest);
        return Application::view('main/home/main',[
            'linkphp' => 'linkphp'
        ]);
        return app()->input('get.');
    }

    public function main()
    {
        function xrange ()
        {
            while (1) {
                $a = (yield '11'); //yield 表达式
                echo $a;
            }
        }
        $a = xrange();//返回一个生成器
        echo $a->current();//返回当前产生的值
        echo '<br>';
        $a->send('33');//向生成器中传入一个值，并且当做 yield 表达式的结果，然后继续执行生成器。
        echo '<br>';
        echo $a->current();
        echo '<br>';
        $a->send('22');
        echo '<br>';
        echo $a->current();
        die;
        function xrange($start, $limit) {
            if ($start < $limit) {
                for ($i = $start; $i <= $limit; ++$i) {
                    yield $i;
                }
            }
        }
        foreach (xrange(1, 9) as $number) {
            echo "$number ";
        }
        die;
//        dump($gen);die;
        $count = Db::table('lp_user')->count('id');
        $page = new Paginator($count,2);
        $data = Db::table('lp_user')->limit($page->limit())->select();
        $page->render();
        foreach ($data as $row) {
            var_dump($row);
        }
//        dump($data);die;
        return Db::select(['select * from lp_user where id > ? and id < ?',[1,3]])->get();
//        dump($httpRequest);
//        dump(app()->input('get.'));
//        Validator::data('www.linkphp.com/1/100') //需要验证的数据集合
//            ->withValidator('url', function ($validator, $input){ //使用验证器闭包
//                $validator->addValidator($input,[//添加验证器规则信息
//                    'rule' => [
//                        'class' => 'url', 'param' => []
//                    ], 'errorMessage' => '非法URL地址'
//                ]);
//            });
//        //检测数据
//        if(!Validator::check()){
//            //出错获取错误信息
//            dump(Validator::geterror());die;
//        }
//        dump(true);die;
//        dump(app()->input('get.'));die;
//        Router::get('/index/index/index', '/index/api/index');
//        Router::post('/index/index/index', '/index/api/index');
//        Router::delete('/index/index/index', '/index/api/index');
//        Router::patch('/index/index/index', '/index/api/index');
//        Router::put('/index/index/index', '/index/api/index');
//        throw new Exception('test');
//        dump(new PhpRpcClient());die;
//        dump(Config::get('app.app_debug'));
//        return 'linkphp start';
//        return ['code' => 1, 'msg' => 'linkphp start'];
//        $filename = ROOT_PATH . 'src/resource/view/main/home/main.html';
//        return file_get_contents($filename);
//        Application::view('main/home/main',[
//            'linkphp' => 'linkphp'
//        ]);
//        dump(app()->getContainerInstance());die;
//        dump(Config::get(''));die;
//        dump(Db::table('lp_download')->sum('id'));die;
//        dump(Db::table('lp_download')->count('id'));die;
//        dump(Db::select('select * from lp_download a
//left join lp_down_item b on a.id = b.down_id
//left join lp_user c on a.u_id = c.id where a.id = ?',[1])->get());die;
        return Db::table('lp_download a')
            ->join('left join lp_down_item b on a.id = b.down_id')
//            ->join('left join lp_user c on a.u_id = c.id')
            ->leftJoin('lp_user c on a.u_id = c.id')
            ->where('a.id>1')
            ->select();
        dump(Db::table('lp_download a')
            ->join('left join lp_down_item b on a.id = b.down_id')
//            ->join('left join lp_user c on a.u_id = c.id')
            ->leftJoin('lp_user c on a.u_id = c.id')
            ->where('a.id>1')
            ->select());
            dump(Db::table('lp_download')->getLastSql());die;
//        dump(Db::select('select * from lp_user where id = ?',[1])->get());die;
//        dump(Db::table('lp_user')->where('id=3')->delete());die;
//        dump(Db::table('lp_user')->where('id = 1')->setInc('pass_word'));die;
//        dump(Db::table('lp_user')->where('id=1')->update(
//            ['user_name' => 'bananabook','pass_word' => '123']
//        ));
//        dump(Db::getTable());die;
        dump(Db::table('lp_user')->field('id')->where('id = 1')->find());
        dump(Db::table('lp_user')->field('id')->where('id = 1')->select());die;
//        dump(Application::db()->select('select id from lp_forum '));die;
//        dump(Application::db()->table('lp_forum')->field('id')->select());die;
        return ['code' => 1,'msg' => 'test'];
        dump(confirm_zeeyer_compiled('zeeyer'));die;
        dump($this->view('main/home/main',['linkphp' => 'linkphp']));die;
        dump(request());
        dump(db());die;
        dump(Application::db());die;
        dump(Application::cache('test'));die;
        dump(Application::cache('test','test'));die;
        Application::view('main/home/main',[
            'linkphp' => 'linkphp'
        ]);die;
        dump(Db::table('zq_user')->where('id=11')->update());die;
        dump(Db::table('zq_user')->where('id=11')->delete());die;
        dump(Db::table('zq_user')->insertAll([['id' => 1,'test' => 'test'],['id' => 1,'test' => 'test']]));die;
        dump(Db::insert('insert into zq_user id values 1'));die;
        dump(Db::table('zq_user')->insert(['id' => 1]));die;
        dump(Db::table('zq_user')->field('id')->where('id = 11')->select());die;
        dump(Db::table('zq_user')->getLastSql());die;
        dump(Db::select('select * from zq_user'));
        Application::event(
            'test',
            [
                \app\controller\main\Event::class,
                \app\controller\main\Event::class,
                \app\controller\main\Event::class
            ]
        );
        Application::event('test');
        Application::router('index/getUser',function(){
            return 1;
        });
        dump(Application::config()->getLoadPath());
        Application::middleware('beginMiddleware',function (Closure $v) {
            $v();
            echo 3;
            return $v;
        });
        Application::middleware('beginMiddleware',function (Closure $v) {
            $v();
            echo 4;
            return $v;
        });
        Application::middleware('beginMiddleware',function (Closure $v) {
            $v();
            echo 5;
            return $v;
        });
        Application::middleware('beginMiddleware',function (Closure $v) {
            $v();
            echo 6;
            return $v;
        });
        Application::middleware('beginMiddleware',function (Closure $v) {
            $v();
            echo 7;
            return $v;
        });
        Application::middleware('beginMiddleware');die;
//        Application::input('get.in',function($value){
//            //闭包实现
//            //这里写具体的过滤方法
//            //自定义
//            //记得返回处理好的
//            return $value;
//        });
//        dump(Application::httpRequest()->isGet());
	}
}